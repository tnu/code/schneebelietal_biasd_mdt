
%%% Master Script for BIASD SUT task 
% This script runs on MATLAB vR2016b
% This is the main script for the results of the Manuscript

% DISENTANGLING BAYESIAN THEORIES 
% IN INDIVIDUALS WITH AUTISM SPECTRUM DISORDER

% Maya Schneebeli, Helene Haker, Annia R�esch, Nicole Zahnd, Stephanie 
% Marino, Gina Paolini, Frederike Petzschner, Klaas Enno Stephan

% Author: Maya Schneebeli 
% Reviewer: Annia R�esch

% Copyright (C) 2020 TNU, Institute for Biomedical Engineering, University of Zurich and ETH Zurich.

% This file is released under the terms of the GNU General Public
% Licence (GPL), version 3. You can redistribute it and/or modify it under the terms of the GPL
% (either version 3 or, at your option, any later version). 
%
% This program is distributed in the hope that it will be useful,
% but WITHOUT ANY WARRANTY; without even the implied warranty of
% MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
% GNU General Public License for more details.
%
% You should have received a copy of the GNU General Public License
% along with this program.  If not, see <http://www.gnu.org/licenses/>.


disp('This code produces the results of the paper:')
disp(' ')
disp('DISENTANGLING BAYESIAN THEORIES IN')
disp('INDIVIDUALS WITH AUTISM SPECTRUM DISORDER')
disp(' ')
disp('Contact: maya.schneebeli@uzh.ch')
disp('------------------------------------------')

rng(54321)


%Navigate to the folder '...\biasd_mdt\code'
cd([pwd, '\biasd_code\code']);

diary([pwd,'\BIASD_SUT_results_matlab.txt'])
diary on;

% Set paths and subject IDs 
OPTIONS = biasd_set_global_options_A;

% Prior to each analysis step use biasd_setOptions to 
% define the location of the input and output file:

% OPTIONS = biasd_setOptions(OPTIONS, 'startfolder', 'startfile', ...
%   'analysisfolder', 'analysisfile');
% takes the input file from /analysis/BIASD_{id}/{startfolder}/{startfile}
% and save the output file in /analysis/BIASD_{id}/{analysisfolder}/{analysisfile}


%% Preprocessing & add covariates to data frame
disp(' ')
disp('///////////////')
disp('OUTLIER REMOVAL')
disp('///////////////')
disp(' ')

 
for idcell = OPTIONS.subjectIDsAll
    
      
    id = [char(idcell)];
    
    % remove outliers
    OPTIONS = biasd_setOptions(OPTIONS, 'preprocessing', 'Draw_covariates', 'preprocessing', 'Dcorr');
    D = biasd_remove_outlier(id,OPTIONS);
 
end


%% Identify excluded subjects
% combine all data to one big data frame
diary off;
OPTIONS.analysisstepsdir = {'preprocessing'};
OPTIONS.analysisstepsfile = {'Dcorr'};
OPTIONS = biasd_setOptions(OPTIONS, '', '', 'preprocessing', 'Dcorr_all');
[allD, D,P] = biasd_combine_data(OPTIONS,'subjectIDsAll');
diary on;
%%
% identify excluded subjects
OPTIONS = biasd_setOptions(OPTIONS, 'preprocessing', 'Dcorr_all', 'results', 'excluded1');
[allD, data, excludedIDs1] = biasd_plot_per_subject(OPTIONS,'subjectIDsAll',{'correct','missing'}, {'coh'}, {[.512]}, [.80, 0.2],{'s','l'});

OPTIONS = biasd_setOptions(OPTIONS, 'preprocessing', 'Dcorr_all', 'results', 'excluded2');
[allD, data, excludedIDs2] = biasd_plot_per_subject(OPTIONS,'subjectIDsAll',{'correct','missing'}, {}, [], [.65, 0.1],{'s','l'});

excludedIDs = unique([excludedIDs1; excludedIDs2]);
disp(' ')
disp('These IDs were excluded from further analyses:')
disp(excludedIDs)

% % set OPTIONS.subjectIDs as array with included IDs
% OPTIONS.subjectIDs = OPTIONS.subjectIDsAll(~ismember(str2double(OPTIONS.subjectIDsAll), excludedIDs));
% OPTIONS.CIDs = OPTIONS.CIDs(~ismember(str2double(OPTIONS.CIDs), excludedIDs));
% OPTIONS.ASDIDs = OPTIONS.ASDIDs(~ismember(str2double(OPTIONS.ASDIDs), excludedIDs));


id='subjectIDs';
CGid = 'CGIDs';
AGid = 'AGIDs';

diary off;
% combine data from included subjects
OPTIONS = biasd_setOptions(OPTIONS, '', '', 'preprocessing', 'Dcorr');
[allD, D,P] = biasd_combine_data(OPTIONS,id);

% generate mean Values for both groups for plots (later)
OPTIONS = biasd_setOptions(OPTIONS, '', '', 'preprocessing', 'Dcorr');
[allD, D,P] = biasd_combine_data(OPTIONS,CGid);

OPTIONS = biasd_setOptions(OPTIONS, '', '', 'preprocessing', 'Dcorr');
[allD, D,P] = biasd_combine_data(OPTIONS,AGid);

diary on;
%% TABLE 1: Demographics
%make tables and plots from demographic statistics 
%i.e. Questionnaires, sociodemographics

disp('//////////////////////')
disp('TABLE 1: Demographics')
disp('//////////////////////')

%biasd_plot plots and prepares the data for biasd_test. 

% plot descriptives
OPTIONS = biasd_setOptions(OPTIONS, 'preprocessing', 'Dcorr', 'results', 'demographics');
[x, data,~]=biasd_plot(OPTIONS,id,{'hasASD'}, {'dummy'},{'male',...
     'age', 'righthand', 'AQ_score', 'zst_total', 'mosaik','rechnen','d2_f'}, ...
     {'task'}, [1], {'none'}, 3, 1);

% get values for demographics for whole sample (data)
OPTIONS = biasd_setOptions(OPTIONS, 'preprocessing', 'Dcorr', 'results', 'demographics_overall');
[~, data,~] = biasd_plot(OPTIONS,id,{'dummy'}, {'task'},{'male',...
     'age', 'righthand', 'AQ_score', 'zst_total', 'mosaik','rechnen','d2_f'}, {'task'}, [1], {'none'}, 3, 1);

% get overview table for demographics for whole sample (summary)
%summary provides means and test statistics for all performed tests. 
%means and std of groups are shown in acending sequence, i.e. for 'hasASD' C
%is the first value und ASD the second value. 

OPTIONS = biasd_setOptions(OPTIONS, 'results', 'demographics', 'results', 'demographics');
[~, ~, summary] = biasd_test(OPTIONS,id,{'hasASD'},{'male',...
     'age', 'righthand', 'AQ_score', 'zst_total', 'mosaik','rechnen','d2_f'}, ...
     {'chi2', 'ttest', 'chi2', 'ranksum', 'ranksum', 'ranksum', 'ttest', 'ttest'});

% TABLE 1 overview output in command window
d_mean = stack(data, [4:5:43],'NewDataVariableName','mean', 'IndexVariableName','variable');
d_std = stack(data, [6:5:43],'NewDataVariableName','std', 'IndexVariableName','variable');

disp('----------------------')
disp('Values for whole group')
disp('----------------------')

summary_all = table(d_mean.variable, d_mean.mean, d_std.std, 'VariableNames', {'measure', 'mean', 'std'})

disp('-------------------------------------')
disp('Values for groups and test statistics')
disp('-------------------------------------')

summary_tests = summary


%% Sanity Checks
% make tables and plots from descriptive statistics
% i.e. task performance etc.
disp('///////////////////////////////////////////////////////////////////////////////')
disp('Supplementary results: Overall effects of stability, validity and expectedness')
disp('///////////////////////////////////////////////////////////////////////////////')

disp(' ')

disp('---------------')
disp('Effects of task')
disp('---------------')

disp(' ')

OPTIONS = biasd_setOptions(OPTIONS, 'preprocessing', 'Dcorr', 'sanity_checks', 'task_effect');
biasd_plot(OPTIONS,id,{'dummy'}, {'task'},{'correct50zero', 'rt'}, {'coh'}, {[0 0.032,0.128,0.512]}, {'none'}, 3,1);
OPTIONS = biasd_setOptions(OPTIONS, 'sanity_checks', 'task_effect', 'sanity_checks', 'task_effect');
[~, ~,f] = biasd_test(OPTIONS,id,{'task'},{'correct50zero', 'rt'}, ...
    {'ttest_p', 'ttest_p'});

disp(' ')

disp(f)

disp(' ')

% differences in expected/unexpected
disp(' ')

disp('-----------------------')
disp('Effects of expectedness')
disp('-----------------------')

OPTIONS = biasd_setOptions(OPTIONS, 'preprocessing', 'Dcorr', 'sanity_checks', 'expected_effect');
biasd_plot(OPTIONS,id,{'expected'}, {'dummy'},{'correct50zero', 'rt', 'phase_consistency','missing','outlier'}, {'task'}, [2], {'none'}, 3,1);
OPTIONS = biasd_setOptions(OPTIONS, 'sanity_checks', 'expected_effect', 'sanity_checks', 'expected_effect');
[~, ~,f] = biasd_test(OPTIONS,id,{'expected'},{'correct50zero', 'rt', 'phase_consistency','missing','outlier'}, ...
    {'signrank', 'ttest_p', 'signrank', 'signrank','signrank','signrank'});

disp(' ')

disp(f)

% Validity (question)
disp(' ')
disp('------------------------------')
disp('Effects of validity (question)')
disp('------------------------------')

OPTIONS = biasd_setOptions(OPTIONS, 'preprocessing', 'Dcorr', 'sanity_checks', 'phase_validity_question');
biasd_plot(OPTIONS,id,{'phasevalidity'}, {'dummy'},{'reported_cue_consistency'}, {'task', 'isquestion'}, [2,1], {'none'}, 3, 1);
OPTIONS = biasd_setOptions(OPTIONS, 'sanity_checks', 'phase_validity_question', 'sanity_checks', 'phase_validity_question');
[~, ~,f] = biasd_test(OPTIONS,id,{'phasevalidity'},{'reported_cue_consistency'}, ...
    {'ttest_p'});

disp(' ')
disp(f)


%Validity (choice) in zero coherence trials
disp(' ')
disp('----------------------------')
disp('Effects of validity (choice)')
disp('----------------------------')
OPTIONS = biasd_setOptions(OPTIONS, 'preprocessing', 'Dcorr', 'sanity_checks', 'phase_validity_choice');
[statarray, data,f] = biasd_plot(OPTIONS,id,{'phasevalidity'}, {'dummy'},{'cue_consistency'}, {'task', 'coh'}, [2,0], {'none'}, 3, 1);
OPTIONS = biasd_setOptions(OPTIONS, 'sanity_checks', 'phase_validity_choice', 'sanity_checks', 'phase_validity_choice');
[statarray, data,f] = biasd_test(OPTIONS,id,{'phasevalidity'},{'cue_consistency'}, ...
    {'ttest_p'});
disp(' ')
disp(f)

% Stability in certainty reports
disp(' ')
disp('--------------------------------')
disp('Effects of stability (certainty)')
disp('--------------------------------')
OPTIONS = biasd_setOptions(OPTIONS, 'preprocessing', 'Dcorr', 'sanity_checks', 'stability_reported_certainty');
[statarray, data,f] = biasd_plot(OPTIONS,id,{'stablephases'}, {'dummy'},{'reported_certainty'}, {'task', 'isquestion'}, [2,1], {'none'}, 3, 1);
OPTIONS = biasd_setOptions(OPTIONS, 'sanity_checks', 'stability_reported_certainty', 'sanity_checks', 'stability_reported_certainty');
[statarray, data,f] = biasd_test(OPTIONS,id,{'stablephases'},{'reported_certainty'}, ...
    {'ttest_p'});

disp(f)

% Stability in reported phase consistency
OPTIONS = biasd_setOptions(OPTIONS, 'preprocessing', 'Dcorr', 'sanity_checks', 'stability_reported_phase_consistency');
biasd_plot(OPTIONS,id,{'stablephases'}, {'dummy'},{'reported_phase_consistency'}, {'task', 'isquestion'}, [2,1], {'none'}, 3, 1);
OPTIONS = biasd_setOptions(OPTIONS, 'sanity_checks', 'stability_reported_phase_consistency', 'sanity_checks', 'stability_reported_phase_consistency');
[~, ~,f] = biasd_test(OPTIONS,id,{'stablephases'},{'reported_phase_consistency'}, ...
    {'ttest_p'});

disp('--------------------------------------------')
disp('Effects of stability (rep phase consistency)')
disp('--------------------------------------------')
disp(f)

% Stability in phase consistency
OPTIONS = biasd_setOptions(OPTIONS, 'preprocessing', 'Dcorr', 'sanity_checks', 'stability_choice_phase_consistency');
biasd_plot(OPTIONS,id,{'stablephases'}, {'dummy'},{'phase_consistency'}, {'task', 'coh'}, [2,0], {'none'}, 3, 1);
OPTIONS = biasd_setOptions(OPTIONS, 'sanity_checks', 'stability_choice_phase_consistency', 'sanity_checks', 'stability_choice_phase_consistency');
[~, ~,f] = biasd_test(OPTIONS,id,{'stablephases'},{'phase_consistency'}, ...
    {'ttest_p'});

disp('-----------------------------------------------')
disp('Effects of stability (choice phase consistency)')
disp('-----------------------------------------------')
disp(f)

% relative volatility time point 
disp('---------------------------------------------------------')
disp('Effects of relative volatility time point (rep certainty)')
disp('---------------------------------------------------------')

% ...for reported certainty
OPTIONS = biasd_setOptions(OPTIONS, 'preprocessing', 'Dcorr', 'sanity_checks', 'timepoint_reported_certainty');
biasd_plot(OPTIONS,id,{'beforePh'}, {'dummy'},{'reported_certainty'}, {'task', 'isquestion', 'beforePh'}, {[2],[1],[1,2]}, {'none'}, 3, 1);
OPTIONS = biasd_setOptions(OPTIONS, 'sanity_checks', 'timepoint_reported_certainty', 'sanity_checks', 'timepoint_reported_certainty');
[~, ~,f] = biasd_test(OPTIONS,id,{'beforePh'},{'reported_certainty'}, ...
    {'signrank'});

disp(f)
% ...for reported phase consistency
disp('-----------------------------------------------------------------')
disp('Effects of relative volatility time point (rep phase consistency)')
disp('-----------------------------------------------------------------')

OPTIONS = biasd_setOptions(OPTIONS, 'preprocessing', 'Dcorr', 'sanity_checks', 'timepoint_reported_phase_consistency');
[statarray, data,f] = biasd_plot(OPTIONS,id,{'beforePh'}, {'dummy'},{'reported_phase_consistency'}, {'task', 'isquestion', 'beforePh'}, {[2],[1],[1,2]}, {'none'}, 3, 1);
OPTIONS = biasd_setOptions(OPTIONS, 'sanity_checks', 'timepoint_reported_phase_consistency', 'sanity_checks', 'timepoint_reported_phase_consistency');
[statarray, data,f] = biasd_test(OPTIONS,id,{'beforePh'},{'reported_phase_consistency'}, ...
    {'signrank'});

disp(f)

% ...for choice phase consistency
disp('-----------------------------------------------------------------')
disp('Effects of relative volatility time point (choice ph consistency)')
disp('-----------------------------------------------------------------')

OPTIONS = biasd_setOptions(OPTIONS, 'preprocessing', 'Dcorr', 'sanity_checks', 'timepoint_choice_phase_consistency');
[statarray, data,f] = biasd_plot(OPTIONS,id,{'beforePh'}, {'dummy'},{'phase_consistency'}, {'task', 'beforePh', 'coh'}, {[2],[1,2],[0]}, {'none'}, 3, 1);
OPTIONS = biasd_setOptions(OPTIONS, 'sanity_checks', 'timepoint_choice_phase_consistency', 'sanity_checks', 'timepoint_choice_phase_consistency');
[statarray, data,f] = biasd_test(OPTIONS,id,{'beforePh'},{'phase_consistency'}, ...
    {'ttest_p'});
disp(f)

%% TABLE 2 & 3: Performance (Correct & RT) 
%(DDM parameters see below)
disp('//////////////////////////////////////////////////////////////')
disp('TABLE 2: Performance & Supplementary material: Outlier removal')
disp('//////////////////////////////////////////////////////////////')

disp('------------------------------------------------')
disp('Task 1: Percent correct, reaction time, missings')
disp('------------------------------------------------')

% values for whole sample
OPTIONS = biasd_setOptions(OPTIONS, 'preprocessing', 'Dcorr', 'results', 'performance_all_both_tasks');
biasd_plot(OPTIONS,id,{'task'}, {'dummy'},{'correct_p','rt','missing'}, {}, [], {'none'}, 3, 1);
OPTIONS = biasd_setOptions(OPTIONS, 'results', 'performance_all_both_tasks', 'results', 'table2_performance');
[~, ~,summary] = biasd_test(OPTIONS,id,{'task'},{'correct_p', 'rt','missing'}, {'ttest', 'ttest','signrank'});

disp(summary)

disp('Missings: Supplementary Outlier removal')
disp(['Task 1 #missings: ', num2str(280*summary.mean_task_1_2(3, 1))]); 
disp(['Task 2 #missings: ', num2str(266*summary.mean_task_1_2(3,2))]); 



% Task 1: patients/controls 
OPTIONS = biasd_setOptions(OPTIONS, 'preprocessing', 'Dcorr', 'results', 'performance_groups_task1');
biasd_plot(OPTIONS,id,{'hasASD'}, {'task'},{'correct_p','rt','missing'}, {'task'}, [1], {'none'}, 3, 1,.004);%,{[50,100],[.4,1.2]}
OPTIONS = biasd_setOptions(OPTIONS, 'results', 'performance_groups_task1', 'results', 'performance_groups_task1');
[~, ~,summary1] = biasd_test(OPTIONS,id,{'hasASD'},{'correct_p', 'rt','missing'}, {'ttest','ttest','ranksum'});

disp(summary1)

% Task 2: patients/controls
disp('------------------------------------------------')
disp('Task 2: Percent correct, reaction time, missings')
disp('------------------------------------------------')

OPTIONS = biasd_setOptions(OPTIONS, 'preprocessing', 'Dcorr', 'results', 'performance_groups_task2');
biasd_plot(OPTIONS,id,{'hasASD'}, {'task'},{'correct_p','missing','rt'}, {'task'}, [2], {'none'}, 3, 1,.004,{[.5,1],[0,.04],[.4,1.2]});
OPTIONS = biasd_setOptions(OPTIONS, 'results', 'performance_groups_task2', 'results', 'performance_groups_task2');
[~, ~,summary2] = biasd_test(OPTIONS,id,{'hasASD'},{'correct_p','rt','missing'}, {'ttest', 'ttest','ranksum'});

disp(summary2)
% Table 2: Summary statistics performance




%% H2: Imprecise prior hypothesis 
disp('//////////////////////////////')
disp('H2: Imprecise prior hypothesis')
disp('//////////////////////////////')

disp(' ')
disp('H2a, H2b: plots only, for statistical analyses see R script (biasd_master_r')
%H2a: correct~task*patient, rt~task*patient (test statistics in R)
OPTIONS = biasd_setOptions(OPTIONS, 'preprocessing', 'Dcorr', 'figures', 'H2a_task_coh_group');
[allD, P] = biasd_plot(OPTIONS,id, {'hasASD', 'task'}, {'coh'}, {'rt', 'correct'},{'coh'}, {[.032, .128,.512]},{'-'}, 30,0 ,.005);

%H2b: rt/correct~expected*patient (test statistics in R)
%------------- Figure 5A --------------------
OPTIONS = biasd_setOptions(OPTIONS, 'preprocessing', 'Dcorr', 'figures', 'H2b_exp_coh_group');
[~,~,f,~] = biasd_plot(OPTIONS,id, {'hasASD'}, {'expected'}, {'rt', 'correct_p'},{'task','coh'}, {[2], [.032,.128,.512]},{'none'}, 1,1, .4,0, 1);
f.Position=[0.0906    0.3479    0.6863    0.3937];

%H2c: question reports
% -------------Figure 5B --------------------
% Reported phase consistency
disp(' ')
disp('-------------------------------')
disp('H2c: Reported phase consistency')
disp('-------------------------------')

OPTIONS = biasd_setOptions(OPTIONS, 'preprocessing', 'Dcorr', 'h2_imprecise_prior', 'reported_phase_consistency');
[~,~,f,~]=biasd_plot(OPTIONS,id,{'hasASD'}, {'dummy'},{'reported_phase_consistency'}, {'task', 'isquestion'}, [2,1], {'none'}, 1, 1, .4,0,1 );
f.Position=[ 0.0906    0.3479    0.1715    0.3937];
xlim([0.3, 1.3])
OPTIONS = biasd_setOptions(OPTIONS, 'h2_imprecise_prior', 'reported_phase_consistency', 'h2_imprecise_prior', 'reported_phase_consistency');
[~, ~,f] = biasd_test(OPTIONS,id,{'hasASD'},{'reported_phase_consistency'}, ...
    {'ttest'});


disp(f)


% Reported certainty
disp(' ')
disp('-----------------------')
disp('H2c: reported certainty')
disp('-----------------------')

OPTIONS = biasd_setOptions(OPTIONS, 'preprocessing', 'Dcorr', 'h2_imprecise_prior', 'reported_certainty');
[~,~,f,~]=biasd_plot(OPTIONS,id,{'hasASD'}, {'dummy'},{'reported_certainty'}, {'task', 'isquestion'}, [2,1], {'none'}, 1, 1, .4,0,1);
f.Position=[ 0.0906    0.3479    0.1715    0.3937];
xlim([0.3, 1.3])
OPTIONS = biasd_setOptions(OPTIONS, 'h2_imprecise_prior', 'reported_certainty', 'h2_imprecise_prior', 'reported_certainty');
[~, ~,f] = biasd_test(OPTIONS,id,{'hasASD'},{'reported_certainty'}, ...
    {'ranksum'});

disp(f)





% choice phase consistency
disp(' ')
disp('-----------------------------')
disp('H2c: choice phase consistency')
disp('-----------------------------')
OPTIONS = biasd_setOptions(OPTIONS, 'preprocessing', 'Dcorr', 'h2_imprecise_prior', 'phase_consistency');
[~,~,f,~]=biasd_plot(OPTIONS,id,{'hasASD'}, {'dummy'},{'phase_consistency'}, {'task', 'coh'}, [2,0], {'none'},  1, 1, .4,0,1);
f.Position=[ 0.0906    0.3479    0.1715    0.3937];
xlim([0.3, 1.3])
OPTIONS = biasd_setOptions(OPTIONS, 'h2_imprecise_prior', 'phase_consistency', 'h2_imprecise_prior', 'phase_consistency');
[~, ~,f] = biasd_test(OPTIONS,id,{'hasASD'},{'phase_consistency'}, ...
    {'ranksum'});

disp(f)

%% H3 Inflexible prior hypothesis 
% cue consistency 
disp(' ')
disp('///////////////////////////////')
disp('H3: Inflexible prior hypothesis')
disp('///////////////////////////////')

disp(' ')
disp('----------------------------')
disp('H3a: cue consistency')
disp('----------------------------')

OPTIONS = biasd_setOptions(OPTIONS, 'preprocessing', 'Dcorr', 'h3_inflexible_prior', 'cue_consistency');
biasd_plot(OPTIONS,id,{'hasASD'}, {'dummy'},{'cue_consistency'}, {'task'}, {[2]}, {'none'}, 3, 1);
OPTIONS = biasd_setOptions(OPTIONS, 'h3_inflexible_prior', 'cue_consistency', 'h3_inflexible_prior', 'cue_consistency');
[~, ~,f] = biasd_test(OPTIONS,id,{'hasASD'},{'cue_consistency'}, ...
    {'ttest'});

disp(f)

disp(' ')
disp('----------------------------')
disp('H3a: reported cue consistency')
disp('----------------------------')


OPTIONS = biasd_setOptions(OPTIONS, 'preprocessing', 'Dcorr', 'h3_inflexible_prior', 'reported_cue_consistency');
biasd_plot(OPTIONS,id,{'hasASD'}, {'dummy'},{'reported_cue_consistency'}, {'task','isquestion'}, {[2],[1]}, {'none'}, 3, 1);
OPTIONS = biasd_setOptions(OPTIONS, 'h3_inflexible_prior', 'reported_cue_consistency', 'h3_inflexible_prior', 'reported_cue_consistency');
[~, ~,f] = biasd_test(OPTIONS,id,{'hasASD'},{'reported_cue_consistency'}, ...
    {'ttest'});

disp(f)

%% H4 Hierarchical prior hypothesis
disp(' ')
disp('/////////////////////////////////')
disp('H4: Hierarchical prior hypothesis')
disp('/////////////////////////////////')

disp(' ')
disp('H4: plots only, for statistical analyses see R script (biasd_master_r')
% Figure 5C
% correct ~ stablephases*expected*patient (test statistics in R)
OPTIONS = biasd_setOptions(OPTIONS, 'preprocessing', 'Dcorr', 'figures', 'Figure_5C');
[~, ~,f] = biasd_plot(OPTIONS,id, {'hasASD', 'expected'}, {'stablephases'},{ 'correct_p'}, {'task'}, [2],{'-', 'none'}, 30,0,.02,{[60,90]});
xlim([0.9,2.1])
% ylim([60,85])
f.Position=[ 0.0906    0.3479    0.1715    0.3937];

% Figure 5D
OPTIONS = biasd_setOptions(OPTIONS, 'preprocessing', 'Dcorr', 'figures', 'validity_time_coh_group_coh0');
[~, ~,f] = biasd_plot(OPTIONS,id, {'hasASD'}, {'stablephases'},{ 'reported_certainty'}, {'task', 'isquestion'}, {[2],[1]},{'none','--'},1, 1, .4,0,1);
xlim([.3,2.3])
f.Position=[ 0.0906    0.3479    0.1871    0.3937];

[~, ~,f] = biasd_plot(OPTIONS,id, {'hasASD'}, {'stablephases'},{ 'reported_phase_consistency'}, {'task', 'isquestion'}, {[2],[1]},{'none','--'}, 1, 1, .4,0,1);
xlim([.3,2.3])
f.Position=[0.0906    0.3479    0.1871    0.3937];

[~, ~,f] = biasd_plot(OPTIONS,id, {'hasASD'}, {'stablephases'},{ 'phase_consistency'}, {'task', 'coh'}, {[2],[0]},{'none','--'}, 1, 1, .4,0,1);
xlim([.3,2.3])
f.Position=[0.0906    0.3479    0.1871    0.3937];






%% Modeling (WFPT)

rng(3333)
for idcell = OPTIONS.(id)

idi = char(idcell)

OPTIONS = biasd_setOptions(OPTIONS, 'preprocessing', 'Dcorr', 'preprocessing', 'Dcorr');
D = biasd_add_variables2(idi, OPTIONS);

    
    % Fit WFPT 
    % v1: no bias (fixed mu1hat). Parameters: ze_v = drift, ze_a =
    % boundary, ze_t = non-decision time.
    
    % 1phases = mu1hat = constant phase bias in valid /invalid phase
    % Parameters: ze_v = drift, ze_a = boundary, ze_t = non-decision time
    % mu1hat = starting point

    
    % v1: no bias (fixed mu1hat = 0.5)
    %fit
    OPTIONS = biasd_setOptions(OPTIONS, 'preprocessing', 'Dcorr', 'psychometric', 'wfptfit_task_v1');
    [D, results] = biasd_fit_wfpt_v1(idi, OPTIONS);

    % 1phases: mu1hat is a constant parameter which is mu1hat for valid
    % phases and 1-mu1hat for invalid phases
   OPTIONS = biasd_setOptions(OPTIONS, 'preprocessing', 'Dcorr', 'psychometric', 'wfptfit_1phase');
   [D] = biasd_fit_1phase(idi, OPTIONS);
    

      disp(['Finished analysis of subject ', idi])
end

%% Combine data
OPTIONS.analysisstepsdir = { 'preprocessing', 'psychometric' ,'psychometric'};
OPTIONS.analysisstepsfile = {'Dcorr','wfptfit_task_v1', 'wfptfit_1phase'};

OPTIONS = biasd_setOptions(OPTIONS, '', '', 'models', 'modelfits');
[allD, D,P] = biasd_combine_data(OPTIONS,id);

OPTIONS = biasd_setOptions(OPTIONS, '', '', 'models', 'modelfits');
[allD, D,P] = biasd_combine_data(OPTIONS,CGid);

OPTIONS = biasd_setOptions(OPTIONS, '', '', 'models', 'modelfits');
[allD, D,P] = biasd_combine_data(OPTIONS,AGid);

%% TABLE 2 & 3 DDM parameters TASK 1
% compare parameters wfpt starting point 0.5 (task 1)
% plot parameters per group

disp('//////////////////////')
disp('MODELING results (DDM)')
disp('//////////////////////')

disp(' ')

disp('-------------------------------------------------')
disp('DDM with no starting point AG vs CG (NCMDT): ')
disp('H1 (Table 1) & Supplementary results (Table S2)')
disp('-------------------------------------------------')

disp(' ')
disp('NOTE:')
disp('ze_a: boundary')
disp('ze_t: non-decision time')
disp('ze_v: drift')
disp('mu1hat: starting point')

OPTIONS = biasd_setOptions(OPTIONS, 'models', 'modelfits', 'models', 'wfpt_v1_task1_parameter');
biasd_plot(OPTIONS,id,{'hasASD'}, {'dummy'},...
    {'wfptfit_task_v1_ze_a','wfptfit_task_v1_ze_v'...
    'wfptfit_task_v1_ze_t'}, {'task','trial'}, {[1],[1]}, {'none'}, 3, 1);
% parameters overall
OPTIONS = biasd_setOptions(OPTIONS, 'models', 'modelfits', 'models', 'wfpt_v1_task1_parameter_all');
[statarray, data,f] = biasd_plot(OPTIONS,id,{'dummy2'}, {'dummy'},...
    {'wfptfit_task_v1_ze_a','wfptfit_task_v1_ze_v'...
    'wfptfit_task_v1_ze_t'}, {'task','trial'}, {[1],[1]}, {'none'}, 3, 1);
% test parameters
OPTIONS = biasd_setOptions(OPTIONS, 'models', 'wfpt_v1_task1_parameter','models', 'wfpt_v1_task1_parameter');
[~, ~,f] = biasd_test(OPTIONS,id,{'hasASD'},...
    {'wfptfit_task_v1_ze_a','wfptfit_task_v1_ze_v'...
    'wfptfit_task_v1_ze_t'}, {'ttest','ttest','ttest','ttest','ttest'});


d_mean = stack(data, [4:5:18],'NewDataVariableName','mean', 'IndexVariableName','variable');
d_std = stack(data, [6:5:18],'NewDataVariableName','std', 'IndexVariableName','variable');

%Summary output for TABLE S1%

summary_all = table(d_mean.variable, d_mean.mean, d_std.std, 'VariableNames', {'measure', 'mean', 'std'});
summary_tests = f;

disp(summary_all);
disp(summary_tests);


%% TABLE 2 & 3 DDM parameters TASK 2
% compare parameters wfpt starting point 0.5 (task 2)
% plot parameters per group

disp(' ')

disp('-------------------------------------------------')
disp('DDM with no starting point AG vs CG (CMDT): ')
disp('Supplementary results (Table S2)')
disp('-------------------------------------------------')

disp(' ')
disp('NOTE:')
disp('ze_a: boundary')
disp('ze_t: non-decision time')
disp('ze_v: drift')
disp('mu1hat: starting point')
disp(' ')

OPTIONS = biasd_setOptions(OPTIONS, 'models', 'modelfits', 'models', 'wfpt_v1_task2_parameter');
biasd_plot(OPTIONS,id,{'hasASD'}, {'dummy'},...
    {'wfptfit_task_v1_ze_a','wfptfit_task_v1_ze_v'...
    'wfptfit_task_v1_ze_t'}, {'task','trial'}, {[2],[1]}, {'none'}, 3, 1);
% parameters overall
OPTIONS = biasd_setOptions(OPTIONS, 'models', 'modelfits', 'models', 'wfpt_v1_task2_parameter_all');
[~, data,~] = biasd_plot(OPTIONS,id,{'dummy2'}, {'dummy'},...
    {'wfptfit_task_v1_ze_a','wfptfit_task_v1_ze_v'...
    'wfptfit_task_v1_ze_t'}, {'task','trial'}, {[2],[1]}, {'none'}, 3, 1);
% test parameters
OPTIONS = biasd_setOptions(OPTIONS, 'models', 'wfpt_v1_task2_parameter','models', 'wfpt_v1_task2_parameter');
[~, ~,f] = biasd_test(OPTIONS,id,{'hasASD'},...
    {'wfptfit_task_v1_ze_a','wfptfit_task_v1_ze_v'...
    'wfptfit_task_v1_ze_t'}, {'ttest','ttest','ttest','ttest','ttest'});


d_mean = stack(data, [4:5:18],'NewDataVariableName','mean', 'IndexVariableName','variable');
d_std = stack(data, [6:5:18],'NewDataVariableName','std', 'IndexVariableName','variable');

%Summary output for TABLE S1%

summary_all = table(d_mean.variable, d_mean.mean, d_std.std, 'VariableNames', {'measure', 'mean', 'std'})
summary_tests = f


%% compare DDM parameters across tasks
disp(' ')
disp('-------------------------------------------------')
disp('DDM with no starting point NCMDT vs CMDT: ')
disp('Supplementary results (Table S1)')
disp('-------------------------------------------------')

disp(' ')
disp('NOTE:')
disp('ze_a: boundary')
disp('ze_t: non-decision time')
disp('ze_v: drift')
disp(' ')

OPTIONS = biasd_setOptions(OPTIONS, 'models', 'modelfits', 'models', 'wfpt_v1_parameter');
biasd_plot(OPTIONS,id,{'task'}, {'dummy'},...
    {'wfptfit_task_v1_ze_a','wfptfit_task_v1_ze_v'...
    'wfptfit_task_v1_ze_t'}, {'trial'}, {[1]}, {'none'}, 3, 1);
% test parameters
OPTIONS = biasd_setOptions(OPTIONS, 'models', 'wfpt_v1_parameter','models', 'wfpt_v1_parameter');
[~, ~,f] = biasd_test(OPTIONS,id,{'task'},...
    {'wfptfit_task_v1_ze_a','wfptfit_task_v1_ze_v'...
    'wfptfit_task_v1_ze_t'}, {'ttest','ttest','ttest','ttest','ttest'});




%% TABLE S 2 (DDM with fitted starting point)
disp(' ')
disp('-------------------------------------------------')
disp('DDM with fitted starting point AG vs CG (CMDT): ')
disp('Supplementary results (Table S2)')
disp('-------------------------------------------------')

disp(' ')
disp('NOTE:')
disp('ze_a: boundary')
disp('ze_t: non-decision time')
disp('ze_v: drift')
disp('mu1hat: starting point')
disp(' ')

OPTIONS = biasd_setOptions(OPTIONS, 'models', 'modelfits', 'models', 'wfpt_1phases_parameter');
biasd_plot(OPTIONS,id,{'hasASD'}, {'dummy'},...
    {'wfptfit_1phases_ze_a','wfptfit_1phases_ze_v'...
    'wfptfit_1phases_ze_t','wfptfit_1phases_mu1hat'}, {'task','trial'}, {[2],[1]}, {'none'}, 3, 1);
% parameters overall
OPTIONS = biasd_setOptions(OPTIONS, 'models', 'modelfits', 'models', 'wfpt_1phases_parameter_all');
[~, data,~] = biasd_plot(OPTIONS,id,{'dummy2'}, {'dummy'},...
    {'wfptfit_1phases_ze_a','wfptfit_1phases_ze_v','wfptfit_1phases_mu1hat', ...
    'wfptfit_1phases_ze_t'}, {'task','trial'}, {[2],[1]}, {'none'}, 3, 1);
% test parameters
OPTIONS = biasd_setOptions(OPTIONS, 'models', 'wfpt_1phases_parameter','models', 'wfpt_1phases_parameter');
[~, ~,f] = biasd_test(OPTIONS,id,{'hasASD'},...
    {'wfptfit_1phases_ze_a','wfptfit_1phases_ze_v', 'wfptfit_1phases_mu1hat',...
    'wfptfit_1phases_ze_t'}, {'ttest','ttest','ttest','ttest','ttest'});


d_mean = stack(data, [4:5:23],'NewDataVariableName','mean', 'IndexVariableName','variable');
d_std = stack(data, [6:5:23],'NewDataVariableName','std', 'IndexVariableName','variable');

%Summary output for TABLE S1%

summary_all = table(d_mean.variable, d_mean.mean, d_std.std, 'VariableNames', {'measure', 'mean', 'std'})
summary_tests = f

diary off

%% Plot of Figure 3 

disp('////////')
disp('Figure 3')
disp('////////')

% Figure 3G
% only coh level 0.032

%response & rt
OPTIONS = biasd_setOptions(OPTIONS, 'preprocessing', 'Dcorr', 'figures', 'Figure_3_coh_corr_032');
[allD, P, f, sub, colors] = biasd_plot(OPTIONS,id, {'hasASD'}, {'task'}, {'correct_p'},{'coh'}, [0.032], {'none'},45, 0,.1);

% simulate data with average parameter fits for both groups for plots
c=0;
for idcell = {CGid, AGid}
    idi = char(idcell);
    c=c+1;
    OPTIONS = biasd_setOptions(OPTIONS, 'models', 'modelfits', 'psychometric', 'wfpt_continuous_simulation');
     simAll{c} = biasd_simulate_data_wfpt_v1_fittedparms_continuous(idi, OPTIONS);   
end
%%

% Figure 3A,B,D,E
% plot reaction time and response per group for both tasks t 
for t = 1:2
OPTIONS = biasd_setOptions(OPTIONS, 'preprocessing', 'Dcorr', 'figures', ['Figure_3_stim_rt_response_task',num2str(t),'_group_wfptfit']);
[allD, P, f, sub, colors] = biasd_plot(OPTIONS,id, {'hasASD'}, {'stim'}, {'rt', 'response'},{'task'}, [t], {'none'},30, 0,.005);

task = t;
% overlay fit
    for group = 1:2
        hold(sub.s1, 'on');
        plot(sub.s1, simAll{group}{task}.stim, simAll{group}{task}.wfpt_v1_rt_sim*1000,...
            'Color',colors(group,:), 'Linewidth',2,'Linestyle','-') %for ddmfit: simC.xf1, simC.rtrmp1
        xlim([-.6 .6])
        hold(sub.s2, 'on');
        plot(sub.s2, simAll{group}{task}.stim, simAll{group}{task}.wfpt_v1_response_sim*100,...
            'Color',colors(group,:), 'Linewidth',2,'Linestyle','-') %for ddmfit: simC.xf1, simC.rtrmp1
    end

%save figure
details = biasd_subjects(id, OPTIONS);
savefig(details.analysisfig);


% same for correct 
% Figure 3C,F
OPTIONS = biasd_setOptions(OPTIONS, 'preprocessing', 'Dcorr', 'figures', ['Figure_3_stim_correct_task',num2str(t),'_group_wfptfit']);
[allD, P, f, sub, colors] = biasd_plot(OPTIONS,id, {'hasASD'}, {'coh'}, {'correct_p'},{'task'}, [t], {'none'},30, 0,.002);
for task = t
    for group = 1:2
        hold(sub.s1, 'on');
        plot(sub.s1, simAll{group}{task}.coh, 100*simAll{group}{task}.wfpt_v1_correct_sim,...
            'Color',colors(group,:), 'Linewidth',2,'Linestyle','-') 
        
    end
end
details = biasd_subjects(id, OPTIONS);
savefig(details.analysisfig);
end

%% Figure S1
disp('//////////')
disp('Figure S 1')
disp('//////////')
% rt & response & correct for all subjects per task with WFPT

c=0;
for idcell = {CGid, AGid}
    idi = char(idcell);
    c=c+1;
    OPTIONS = biasd_setOptions(OPTIONS, 'models', 'modelfits', 'psychometric', 'wfpt_continuous_simulation');
     simAll{c} = biasd_simulate_data_wfpt_v1_fittedparms_continuous(idi, OPTIONS);   
end

%response & rt

%%

OPTIONS = biasd_setOptions(OPTIONS, 'preprocessing', 'Dcorr', 'figures', ['stim_rt_response_wfptfit']);
[allD, P, f, sub, colors] = biasd_plot(OPTIONS,id, {'task'}, {'stim'}, {'rt', 'response'},{}, [], {'none'},[25,2,0], 0,.00);
for t = 1:2
task = t;
    for group = 1
        hold(sub.s1, 'on');
        plot(sub.s1, [simAll{1}{task}.stim], ...
            [simAll{1}{task}.wfpt_v1_rt_sim'*1000],...
            'Color',colors(task,:), 'Linewidth',2,'Linestyle','-') %for ddmfit: simC.xf1, simC.rtrmp1
        xlim([-.6 .6])
        hold(sub.s2, 'on');
        plot(sub.s2, [simAll{1}{task}.stim],...
            [simAll{1}{task}.wfpt_v1_response_sim*100'],...
            'Color',colors(task,:), 'Linewidth',2,'Linestyle','-') %for ddmfit: simC.xf1, simC.rtrmp1
    end
end


details = biasd_subjects(id, OPTIONS);
savefig(details.analysisfig);


% correct 
OPTIONS = biasd_setOptions(OPTIONS, 'preprocessing', 'Dcorr', 'figures', ['stim_correct_wfptfit']);
[allD, P, f, sub, colors] = biasd_plot(OPTIONS,id, {'task'}, {'coh'}, {'correct_p'},{}, [], {'none'},[25,2,0], 0,.00);
for task = 1:2
    for group = 1
        hold(sub.s1, 'on');
        plot(sub.s1, [simAll{1}{task}.coh], ...
            100*[simAll{1}{task}.wfpt_v1_correct_sim],...
            'Color',colors(task,:), 'Linewidth',2,'Linestyle','-') %for ddmfit: simC.xf1, simC.rtrmp1
        
    end
end
details = biasd_subjects(id, OPTIONS);
savefig(details.analysisfig);

%%

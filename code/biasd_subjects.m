function [ details] = biasd_subjects( id , OPTIONS)

%INPUT:
% id: ID of subject or subjectgroup
% OPTIONS.analysisdir: preprocessing, psychoFit, modelFit
% OPTIONS.analysis: name of analysis or processing step
% OPTIONS.startdir: preprocessing, (psychoFit, modelFit)
% OPTIONS.start: name of data which enters the analysis step:
%                   Draw, Dcorrected..

%OUTPUT
% Starting file for analysis
   % details.startfile

% Analysed file (dependent on OPTIONS.task, OPTIONS.analysisdir and OPTIONS.analysis):
   % details.analysisfile
   % details.analysisfig
  
  
% Author: Maya Schneebeli 
% Reviewer: Annia R�esch

% Copyright (C) 2020 TNU, Institute for Biomedical Engineering, University of Zurich and ETH Zurich.

% This file is released under the terms of the GNU General Public
% Licence (GPL), version 3. You can redistribute it and/or modify it under the terms of the GPL
% (either version 3 or, at your option, any later version). 
%
% This program is distributed in the hope that it will be useful,
% but WITHOUT ANY WARRANTY; without even the implied warranty of
% MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
% GNU General Public License for more details.
%
% You should have received a copy of the GNU General Public License
% along with this program.  If not, see <http://www.gnu.org/licenses/>.


%% Name
details.subjectname = ['TNU_BIASD_A',id];
disp(['Create paths for subject ' details.subjectname]); 


%% Analysisfile
analysisroot = fullfile(OPTIONS.workdir, 'analysed', details.subjectname);

if isfield(OPTIONS,'analysis')
    analysisname = [details.subjectname,'_', OPTIONS.analysis];
    %for export in R
    details.analysisfile_R = fullfile(analysisroot, OPTIONS.analysisdir, [analysisname, '.csv']);     
    details.analysisfile_R_parms = fullfile(analysisroot, OPTIONS.analysisdir, [analysisname, '_parms.csv']);

    details.pathfilename_R = fullfile(OPTIONS.workdir, 'rcode');
    %name of analysisfile and analysisfigure
    details.analysisfile = fullfile(analysisroot, OPTIONS.analysisdir, [analysisname, '.mat']);
    details.analysisfig = fullfile(analysisroot, OPTIONS.analysisdir, [analysisname, '.fig']);

    if ~exist(fullfile(analysisroot, OPTIONS.analysisdir),'dir')
        mkdir(fullfile(analysisroot, OPTIONS.analysisdir))
    end
    disp(['Outputfile: ' details.analysisfile])
end



%% Inputfile (starfile)

if isfield(OPTIONS, 'start')
startname = [details.subjectname,'_', OPTIONS.start];
details.startfile = fullfile(analysisroot, OPTIONS.startdir, [startname, '.mat']);

disp(['Inputfile: ' details.startfile]);
end





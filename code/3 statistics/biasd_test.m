function [statarray, data, summary] = biasd_plot(OPTIONS,id, group ,depvar, teststat)
%Calculates statistical tests between two groups and returns summary

%INPUT
% - OPTIONS         Struct with path and subject information
% - id              Sample to be tested, usually 'subjectIDs'
% - group           Grouping variable, e.g. 'hasASD'
% - depvar          Dependent variables e.g. {'AQ_score', 'correct'}
% - teststat        statistical Tests to be performed, e.g. {'ranksum', chi2}

                    % ttest: unpaired ttest
                    % ttest_p: paired ttest
                    % chi2: testing proportions for unpaired samples
                    % ranksum: nonparametric test for unpaired samples
                    % signrank: nonparametric test for paired samples

% OUTPUT
% -data             aggregated data
% -summary          summary table (mean, std, test statistics, p-value,ES)

% Author: Maya Schneebeli 
% Reviewer: Annia R�esch

% Copyright (C) 2020 TNU, Institute for Biomedical Engineering, University of Zurich and ETH Zurich.

% This file is released under the terms of the GNU General Public
% Licence (GPL), version 3. You can redistribute it and/or modify it under the terms of the GPL
% (either version 3 or, at your option, any later version). 
%
% This program is distributed in the hope that it will be useful,
% but WITHOUT ANY WARRANTY; without even the implied warranty of
% MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
% GNU General Public License for more details.
%
% You should have received a copy of the GNU General Public License
% along with this program.  If not, see <http://www.gnu.org/licenses/>.

disp(' ')
disp('TEST DATA ...')
disp(['Grouping variable: ' group{1}]);

details = biasd_subjects(id, OPTIONS);
load(details.startfile)

%prepare values for summary
m = []; %mean
st = []; %standard deviation
p_v = []; %p-values
es_v = []; %effect size
sta_v = []; %test statistics value (e.g. t-value)
df_v = []; %degrees of freedom
tests = []; %test

groupvalues = unique(statarray.(group{1}));

if length(groupvalues) > 2
    warning(['Testing categories: ' num2str(groupvalues(1:2))]);
end

for i = 1:length(depvar)
    disp(' ');
    disp(['TEST ' num2str(i), ': '])
    disp(['- Dependent variable: ' depvar{i} , ' / Test statistics: ' teststat{i}])
    
    
    switch teststat{i}
        case 'ttest'
            [F, df1, df2, P]= Levenetest([statarray.(['nanmean_' depvar{i}]), statarray.(group{1})]);
            disp(['Levene-Test for ', depvar{i}, ': F=', num2str(F), '; p = ', num2str(P)]);
            if P<0.05
                warning('samples are NOT normally distributed: ranksum test should applied');
            end
            
            [h p ci stats] = ttest2(statarray.(['nanmean_' depvar{i}])(statarray.(group{1}) ==groupvalues(1)),...
                statarray.(['nanmean_' depvar{i}])(statarray.(group{1}) ==groupvalues(2)));
            es=  computeCohen_d(statarray.(['nanmean_' depvar{i}])(statarray.(group{1}) ==groupvalues(1)),...
                statarray.(['nanmean_' depvar{i}])(statarray.(group{1}) ==groupvalues(2)));
            sta = stats.tstat;
            df = stats.df;
        case 'ttest_p'
            [F, df1, df2, P]= Levenetest([statarray.(['nanmean_' depvar{i}]), statarray.(group{1})]);
            disp(['Levene-Test for ', depvar{i}, ': F=', num2str(F), '; p = ', num2str(P)]);
             if P<0.05
                warning('samples are NOT normally distributed: ranksum test should applied');
            end
            [h p ci stats] = ttest(statarray.(['nanmean_' depvar{i}])(statarray.(group{1}) ==groupvalues(1)),...
                statarray.(['nanmean_' depvar{i}])(statarray.(group{1}) ==groupvalues(2)));
            es=  computeCohen_d(statarray.(['nanmean_' depvar{i}])(statarray.(group{1}) ==groupvalues(1)),...
                statarray.(['nanmean_' depvar{i}])(statarray.(group{1}) ==groupvalues(2)));
            sta = stats.tstat;
            df = stats.df;
        case 'chi2'
            
            [t, ch, p] = crosstab(statarray.(['nanmean_' depvar{i}]).*statarray.GroupCount,...
                statarray.(group{1}));
            
            es = sqrt((ch/height(statarray))/(min(size(t))-1));
            sta = ch;
            df = prod(size(t)-1);
      
            
            
        case 'ranksum'
            %Mann-Whitney-U test
            [F, df1, df2, P]= Levenetest([statarray.(['nanmean_' depvar{i}]), statarray.(group{1})]);
            disp(['Levene-Test for ', depvar{i}, ': F=', num2str(F), '; p = ', num2str(P)]);
            if P>0.05
                warning('samples are normally distributed: t-test can be applied');
            end

            n=length([statarray.(['nanmean_' depvar{i}])(statarray.(group{1}) ==groupvalues(1));...
                statarray.(['nanmean_' depvar{i}])(statarray.(group{1}) ==groupvalues(2))]);
            
            [p,H,stats] = ranksum(statarray.(['nanmean_' depvar{i}])(statarray.(group{1}) ==groupvalues(1)),...
                statarray.(['nanmean_' depvar{i}])(statarray.(group{1}) ==groupvalues(2)));
            if isfield(stats, 'zval')
            es = stats.zval / sqrt(n);
            sta = stats.zval;

            else
                es = NaN;
                sta = NaN;
            end
            
            df = NaN;
            
        case 'signrank'
            [F, df1, df2, P]= Levenetest([statarray.(['nanmean_' depvar{i}]), statarray.(group{1})]);
            disp(['Levene-Test for ', depvar{i}, ': F=', num2str(F), '; p = ', num2str(P)]);
            if P>0.05
                warning('samples are normally distributed: t-test can be applied');
            end

             n=length([statarray.(['nanmean_' depvar{i}])(statarray.(group{1}) ==groupvalues(1));...
                statarray.(['nanmean_' depvar{i}])(statarray.(group{1}) ==groupvalues(2))]);
            
            %Wilcoxon signed rank for paired samples
            [p, h, stats] = signrank(statarray.(['nanmean_' depvar{i}])(statarray.(group{1}) ==groupvalues(1)),...
                statarray.(['nanmean_' depvar{i}])(statarray.(group{1}) ==groupvalues(2)));
            es = stats.zval / sqrt(n);
            
            sta = stats.zval;
            df = NaN;
            
    end

% es=  computeCohen_d(statarray.(['nanmean_' demogr{i}])(statarray.(group{1}) ==groupvalues(1)),...
%                 statarray.(['nanmean_' demogr{i}])(statarray.(group{1}) ==groupvalues(2)));

    data.([depvar{i},'_', teststat{i}, '_p']) = p*ones(height(data),1);
    data.([depvar{i},'_', teststat{i}, '_es']) = es*ones(height(data),1);
    data.([depvar{i},'_', teststat{i}, '_sta']) = sta*ones(height(data),1);
    data.([depvar{i},'_', teststat{i}, '_df']) = df*ones(height(data),1);
%     data.([demogr{i},'_', teststat{i}, '_es']) = es*ones(height(data),1);

    m = [m, data.(['nanmean_nanmean_', depvar{i}])];
    st = [st, data.(['std_nanmean_', depvar{i}])];
    p_v = [p_v, data.([depvar{i},'_', teststat{i}, '_p'])];
    es_v = [es_v, data.([depvar{i},'_', teststat{i}, '_es'])];
    sta_v = [sta_v, data.([depvar{i},'_', teststat{i}, '_sta'])];
    df_v = [df_v, data.([depvar{i},'_', teststat{i}, '_df'])];
    tests = [tests, teststat(i)];
    
    
    
end

summary = table(depvar', m', st', p_v(1,:)', es_v(1,:)', sta_v(1,:)', df_v(1,:)', tests');
summary.Properties.VariableNames = {'name', ['mean_', group{1},'_' ...
    num2str(groupvalues(1)),'_', num2str(groupvalues(2))], ['std_', group{1},'_' ...
    num2str(groupvalues(1)),'_', num2str(groupvalues(2))], 'p', 'es', 'sta' , 'df', 'test'};



save(details.analysisfile, 'data','statarray', 'summary');

end



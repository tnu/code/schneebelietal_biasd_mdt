function [allD, data, excludedIDs] = biasd_plot_per_subject(OPTIONS,id,datavarsf, subsetf, subsetval, cutoff, sl)
%Plots and identifies excluded subjects

% INPUT: 
% - OPTIONS: struct OPTIONS which contains data dirs
% - id          ID group, e.g. subjectIDsALL
% - datavarsf   dependent data variable(s) (y axis)
% - subsetf     cell or char array to plot a certain subset of data
%               name of variables which define subset
% - subsetv     num array to plot certain subset of data
%               values of subsetf, which are included
%               e.g. Evaluate only D.task=1, then subsetf = 'task', 
%               and subsetval = 1; Evaluate only task=1 and coh=0, then 
%               subsetf = {'task', 'coh'} and subsetf = {'1', '0'}
% - cutoff:     cutoff for exclusion array, must have the same length as
%               datavarsf (adds horizontal line to plot)
% - sl:         Defines if participant is excluded if value is smaller ('s') or 
%               larger ('l') than cutoff. String array must have same length as 
%               datavarsf

%Example 
%subsetf = {'task'}
%subsetval = [2];
%datavarsf = {'correct', 'missing'};
%cutoff = [.65, .01];
%sl = {'s', 'l'}

% Author: Maya Schneebeli 
% Reviewer: Annia R�esch

% Copyright (C) 2020 TNU, Institute for Biomedical Engineering, University of Zurich and ETH Zurich.

% This file is released under the terms of the GNU General Public
% Licence (GPL), version 3. You can redistribute it and/or modify it under the terms of the GPL
% (either version 3 or, at your option, any later version). 
%
% This program is distributed in the hope that it will be useful,
% but WITHOUT ANY WARRANTY; without even the implied warranty of
% MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
% GNU General Public License for more details.
%
% You should have received a copy of the GNU General Public License
% along with this program.  If not, see <http://www.gnu.org/licenses/>.


details = biasd_subjects(id, OPTIONS);
load(details.startfile)


excludedIDs = [];


if ~isempty(subsetf)
    if ~iscell(subsetval)
        for i = 1:length(subsetf)
            allD=allD(allD.(subsetf{i}) == subsetval(i),:);
        end
    else
        for i = 1:length(subsetf)
            allD=allD(ismember(allD.(subsetf{i}),subsetval{i}),:);
        end
    end
end

%define group variable as 1 and 2 instead of 0 and 1 for plot
allD.hasASD = allD.hasASD+1;    

color(1,:) =[ 0.4    0.1   0.6];
color(2,:) =[ 0.6    0.1  0.4];


%aggregate Data
method1 = {'nanmean'};
data = grpstats(allD,{'hasASD','id'},method1,...
    'DataVars',datavarsf);
data.ind = (1:height(data))';
data.excluded = zeros(height(data),1);

vars = strcat(method1,'_', datavarsf);

f = figure('units','normalized','outerposition',[0 0 1 1]);


    
for i = 1:length(datavarsf)
subplot(length(datavarsf),1, i)
hold on;
for ip = 1:2
    bar(data.ind(data.hasASD==ip), data.(vars{i})(data.hasASD==ip), 'FaceColor', color(ip,:))
    
end

if strcmp(sl(i),'s')
disp('Subjects below threshold: ')
    disp(data.id(data.(vars{i})<cutoff(i)))
    excludedIDs = [excludedIDs;  data.id(data.(vars{i})<cutoff(i))];
else
    disp('Subjects above threshold: ')
    disp(data.id(data.(vars{i})>cutoff(i)))
    excludedIDs = [excludedIDs;  data.id(data.(vars{i})>cutoff(i))];

end


y = cutoff(i);
line([0,max(data.ind)],[y,y], 'LineWidth', 2, 'Color', 'k')

    
   
    ax = gca;
    ax.XTick = 1:length(data.id);
    ax.XTickLabel = num2str(data.id);
    ax.XTickLabelRotation = 90;
    ylabel(datavarsf(i));

end 

data.excluded = ismember(data.id, excludedIDs);

savefig(details.analysisfig);
save(details.analysisfile, 'data', 'excludedIDs');


end



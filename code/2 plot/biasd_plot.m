function [statarray, data, f, varargout] = biasd_plot(OPTIONS,id, grpvars, xaxisf,datavarsf, subsetf, subsetval, ls, markersize, dobarplot, varargin)
%Flexible plot function

%INPUT
%- OPTIONS        Struct OPTIONS with id groups and path definitions
%- id             id group, usually 'subjectIDs'
%- grpvars        Grouping Variables (e.g. {'hasASD'})
%- xaxisf         Variable on X-Axis (e.g. {'coh'})
%- subsetf        Of which Variable a subset is used for plot (e.g {'task'})
%- subsetval      Define which Values of subsetf-Variable included in plot
%                 (e.g. [2])
%- datavarsf      Dependent variables (e.g. {'correct', 'rt'}), plots subplot
%                 for each dependent variable
%- ls             linestyle for group conditions, e.g. {'-', '--'}
%- markersize     markersize for dots, e.g. 3
%- dobarplot      0 for line plots, 1 for bar plots
%- varargin       offset, ylim

% Author: Maya Schneebeli 
% Reviewer: Annia R�esch

% Copyright (C) 2020 TNU, Institute for Biomedical Engineering, University of Zurich and ETH Zurich.

% This file is released under the terms of the GNU General Public
% Licence (GPL), version 3. You can redistribute it and/or modify it under the terms of the GPL
% (either version 3 or, at your option, any later version). 
%
% This program is distributed in the hope that it will be useful,
% but WITHOUT ANY WARRANTY; without even the implied warranty of
% MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
% GNU General Public License for more details.
%
% You should have received a copy of the GNU General Public License
% along with this program.  If not, see <http://www.gnu.org/licenses/>.

disp('PLOT DATA ...');
details = biasd_subjects(id, OPTIONS);
load(details.startfile)


%% Adjust variable definitions for plotting
%Here, in here if one of the grpvars or xasisf has to be newly defined
%or adjusted to a certain range

%for xaxis: scale variable values such that they have values 1:N
%patient group
allD.hasASD = allD.hasASD + 1;
%phasevalidity = validity of phase
allD.phasevalidity = 3 - (allD.probabilities./.6 + 2/3);
%stablephases = stability of phase
allD.stablephases = 3- (allD.stablephases +1);
%to milliseconds
allD.rt = allD.rt*1000;
allD.reported_certainty = allD.reported_certainty*200;
%correct in percent
allD.correct_p = allD.correct50zero*100;
allD.response = allD.response*100;
%expected = 1 if valid cue in valid phase and invalid cue in invalid phase
allD.expected = allD.expected+1;

%dummy, dummy2, dummy3 = ones for plotting
allD.dummy = ones(height(allD),1);
allD.dummy2 = ones(height(allD),1);
allD.dummy3 = ones(height(allD),1);

allD.phases_trial = repmat([1:224, 1:49, 1:49, ...
    1:14, 1:14, 1:14, 1:14, 1:14, 1:49, 1:49], ...
    1,length(unique(str2num(allD.id))))';

n=25;
allD.beforePh = repmat([nan(224, 1); nan(2*49-n,1); ...
    ones(n,1); nan(70,1); 2*ones(n,1); nan(2*49-n,1)]', ...
    1,length(unique(str2num(allD.id))))';

allD.cue_consistency = allD.cue_consistency *100;
allD.reported_cue_consistency = allD.reported_cue_consistency*100;
allD.reported_phase_consistency = allD.reported_phase_consistency*100;
allD.phase_consistency = allD.phase_consistency*100;

%% Label descriptions
% the label must have the variable name of the variable name in the
% dataframe. E.g. if D.hasASDs should have the label Group, then write
% here: hasASD = 'Group'
wfptfit_phases_mu1hat = 'starting point [sp]';
phases = 'partial sequence';
response = 'proportion rightward (%)';
reported_cue_consistency='reported cue consistency (%)';
phase_consistency='choice state consistency (%)';
rt = 'reaction time [ms]';
stim = 'motion strength (%coh)';
cue_consistency = 'choice cue consistency (%)';
correct_p= 'percent correct (%)';
coh = 'motion strength (%coh)';
reported_phase_consistency = 'reported state consistency (%)';
reported_cue_consistency = 'reported cue consistency (%)';
reported_certainty = 'reported cue confidence (%)';
beforePh = 'relative time point to volatile phase';
dummy = '';
cue_validity={''};
expected={''};
stablephases={''};
AQ_score = 'AQ';
age = 'Age';
zst_total = 'DSST';
male = 'Sex';
rechnen = 'Algebraic';
righthand= 'Handedness';
mosaik = 'Mosaik';
d2_f = 'D2 F';
wfptfit_task_v1_ze_a = 'DDM boundary';
wfptfit_task_v1_ze_v = 'DDM drift';
wfptfit_task_v1_ze_t = 'DDM non-decision time';
wfptfit_1phases_ze_a = 'DDM boundary';
wfptfit_1phases_ze_v = 'DDM drift';
wfptfit_1phases_ze_t = 'DDM non-decision time';
wfptfit_1phases_ze_t = 'DDM starting point';




%% Ticklabel descriptions
% in here if you want to name the conditions differently in the legend
% name the variable for ticklabels as [variablename]tick
% e.g. if you want have the labels 'NT' and 'ASD' for allD.hasASD, then
% write: hasASDtick = {'NT', 'ASD'};

phasevaliditytick = {'valid phase', 'invalid phase'};
stablephasestick = {'stable', 'volatile'};
beforePhtick = {'before volatile phase', 'after volatile phase'};
hasASDtick = {'CG', 'AG'};
probabilitiestick = {'invalid','valid'};
dummytick = {'',''};
dummy2tick = {'',''};
cohtick = {'0','3.2', '12.8', '51.2' };
stimtick = {'-51.2', '-12.8','-3.2','0','3.2', '12.8', '51.2' };
expectedtick = {'unexpected outcome', 'expected outcome'};
cue_validitytick = {'invalid cue', 'valid cue'};
tasktick = {'NCMDT', 'CMDT'};



%% Prepare data frame
% Only take subset
if ~isempty(subsetf)
    if ~iscell(subsetval)
        for i = 1:length(subsetf)
            allD=allD(allD.(subsetf{i}) == subsetval(i),:);
        end
    else
        for i = 1:length(subsetf)
            allD=allD(ismember(allD.(subsetf{i}),subsetval{i}),:);
        end
    end
end

disp(' ')
disp('Grouping Variable: ') 
gvu = unique(allD.(grpvars{1}));
gvv = eval([grpvars{1}, 'tick']);
disp([grpvars{1} ' ' num2str(gvu(1)) ': ' gvv{1}]);
if length(gvu)>1
disp([grpvars{1} ' ' num2str(gvu(2)) ': ' gvv{2}]);
end



%% Prepare color & line styles
%prepare colors
for i = 1:length(grpvars(2:end))
    c{i,:} = unique(allD.(grpvars{i+1}));
end

if length(grpvars) > 1
    ncol = allcomb(c{:});
else
    ncol = 1;
end

ntcol = [];
asdcol=[];


ntcol = [255 15 102]./255;
asdcol = [50 153 204]./255;

if length(ncol) > 1
    ntcol = [ntcol; [255 127 179]./255];
    asdcol = [asdcol; [153 204 231]./255];
end

colors = [ntcol; asdcol];

%define linestyles for more than 2 groupconditions
for i = 1:length(grpvars)
    l{i,:} = unique(allD.(grpvars{i}));
end

nl = allcomb(l{:});
linestyles = [];
if size(nl,2) == 3
    uc = unique(nl(:,3));
    for i = 1:length(nl)
        linestyles = [linestyles,ls(find(uc == nl(i,3)))];
    end
else
    linestyles = repmat(ls, length(nl),1);
end

grpvarsf = [grpvars, xaxisf];

%% aggregate Data

method1 = {'nanmean'};
method2 = {'nanmean', 'sem', 'std', 'sum','conf95'};

statarray = grpstats(allD,[grpvarsf, 'id'],method1,...
    'DataVars',datavarsf);

data = grpstats(statarray,grpvarsf,method2,...
    'DataVars',strcat((method1), '_', datavarsf));

if length(varargin)>2
    doscatter = 1;
else
    doscatter = 0;
end

%% Variable Names
% new variable names of dependent variables, e.g. nanmean_std_correct
for i = 1: length(method2)
    vars(i,:) = strcat(method2(i), '_',method1,'_', datavarsf);
end

% Variable names of combined groups in legend
% E.g. ASD valid, ASD invalid, NT valid, NT invalid
for i = 1:length(grpvars)
    u{i,:} = unique(data.(grpvars{i}));
end

comb = allcomb(u{:});

if doscatter
for i = 1:length(grpvarsf)
    u_sc{i,:} = unique(statarray.(grpvarsf{i}));
end

comb_sc = allcomb(u_sc{:});
end


combnames = cell(length(comb),1);
for i = 1:length(comb)
    combnames{i}=' ';
    
    for j = 1:size(comb,2)
        if exist([grpvars{j},'tick'], 'var')
            gv = eval([grpvars{j},'tick']);
            combnames{i}=[combnames{i}, ' ', gv{comb(i,j)}];
        else
            combnames{i}=[combnames{i}, ' ', grpvars{j},' ', num2str(comb(i,j))];
        end
    end
end



cond = ones(height(data), length(comb));
for j = 1:length(comb)
    for i = 1:length(grpvars)
        cond(:,j) = cond(:,j) & data.(grpvars{i}) == comb(j,i);
    end
end

if doscatter
cond_sc = ones(height(statarray), length(comb_sc));
for j = 1:length(comb_sc)
    for i = 1:length(grpvarsf)
        cond_sc(:,j) = cond_sc(:,j) & statarray.(grpvarsf{i}) == comb_sc(j,i);
    end
end
end

%% Style decisions
if length(varargin)>0
    offset = varargin{1};
elseif dobarplot
    offset = .3;
else
    offset = .002;
end




%% Generate input for plot
% d = all dependent variables
% xd = x values for dependent variables
% m{j,k} = y values for all statistical values j (1=mean, 5=conf95) and
% all subplots k
% x = x values for values in m (xd - offset for each group)

for j = 1:length(method2)
    for k = 1:length(datavarsf)
        m{j,k} = [];
        x = [];
        if j ==1 && doscatter
        end
        for i = 1:length(comb)
            d = data.(vars{j,k});
            xd = data.(xaxisf{1});
            m{j,k} = [m{j,k}, d(logical(cond(:,i)))];
            x = [x-offset, xd(logical(cond(:,i)))];
        end
    end
end

if doscatter
    for k = 1:length(datavarsf)
        scatterd{k} = [];
        
        for i=1:length(comb_sc)  
            scat = statarray.([method1{1}, '_',datavarsf{k}]);

            scatterd{k}=[scatterd{k},{scat(logical(cond_sc(:,i)))}];
        end
    end
end

%% Plot figure
try
f=figure('Name', ['Figure: ' subsetf{1}, num2str(subsetval(1))]);
catch
    f=figure;
end
f.Units = 'normalized';

f.OuterPosition = [0 0 1 1];
f.InnerPosition = [.4 .4 .3 .3];

bcolors = colors;

if length(markersize)>2
    cs = markersize(3);
    lw = markersize(2);
    markersize=markersize(1);
else
    cs = 3;
    lw = 3;
end

for i = 1:length(datavarsf) %one subplot for each variable
    sub.(['s',num2str(i)]) = subplot(1,length(datavarsf), i);
    
    
    
    for j = 1:size(m{1,i},2) %m{1,i} are all means for subplot i
        if dobarplot
            bpl(j) = bar(x(:,j), m{1,i}(:,j), 'FaceColor', bcolors(j,:),...
                'BarLayout', 'grouped','EdgeColor', 'none', 'BarWidth', offset*.8);
            colors(j,:) = [.25 0.25 0.25];
            if doscatter
                hold on;
                for s = 1:size(x,1)
                plot(x(s,j)+0.01*randn(length(scatterd{i}{s+size(m{1,i},1)*(j-1)}),1), scatterd{i}{s+size(m{1,i},1)*(j-1)}, ...
                    '.', 'MarkerSize', 10, 'Color', [.75,.75,.75]);
                end
            end
        end
        hold on;
        if strcmp(linestyles{j}, 'noerr')
            h=plot(x(:,j), m{1,i}(:,j),'.', 'Color', colors(j,:),...
                'MarkerSize', markersize(1))
        else
            
        h=errorbar(x(:,j), m{1,i}(:,j), m{5,i}(:,j),'Color', colors(j,:),...
            'LineWidth', lw, 'LineStyle', linestyles{j}, ...
            'MarkerSize', markersize, 'Marker', '.', 'CapSize', cs);
        end
        
    end
    ax = gca;
    box off;
     ax.XTick = mean(x,2); 
    if exist([xaxisf{1},'tick'],'var')
        set(ax, 'xTickLabel',eval([xaxisf{1},'tick']), 'Fontsize',14);
    end
    
    

    
    
    if exist(datavarsf{i},'var')
        ylabel(eval(datavarsf{i}), 'Fontsize',16);
    else
            ylabel(datavarsf(i), 'Fontsize',20);
    end
    if exist(xaxisf{1},'var')
        xlabel(eval(xaxisf{1}), 'Fontsize',20);
    else
            xlabel(xaxisf, 'Fontsize',20);
    end
    
   
   
    xtickangle(45);
    if length(varargin)>1
        if iscell(varargin{2})      
    ylim(varargin{2}{i})
        end
    end
    axytick = ax.YTick; 
ax.YTick = linspace(axytick(1), axytick(end), 3);
         set(ax, 'yTickLabel',ax.YTickLabel, 'Fontsize',14);


end

if dobarplot
    lgd=legend(bpl,combnames);
else
    lgd = legend(combnames);
end
lgd.Location = 'southeast';
legend boxoff;
% set(f,'Resize','off')
f.OuterPosition=[0 0 1 1];
f.InnerPosition=[.1 .1 .8 .8];

f.Position=[.2 .2 .5 .5];



varargout{1} = sub;
varargout{2} = colors;
varargout{3} = allD;
try
writetable(statarray, details.directfile_R)
catch
end
savefig(details.analysisfig);
save(details.analysisfile, 'data', 'statarray')
%saveas(gcf,details.analysispng);
%   Detailed explanation goes here


end



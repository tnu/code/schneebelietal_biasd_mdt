function [D, irr] = biasd_add_variables2(id, OPTIONS)
% Add variables relevant for analysis

% Author: Maya Schneebeli 
% Reviewer: Annia R�esch

% Copyright (C) 2020 TNU, Institute for Biomedical Engineering, University of Zurich and ETH Zurich.

% This file is released under the terms of the GNU General Public
% Licence (GPL), version 3. You can redistribute it and/or modify it under the terms of the GPL
% (either version 3 or, at your option, any later version). 
%
% This program is distributed in the hope that it will be useful,
% but WITHOUT ANY WARRANTY; without even the implied warranty of
% MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
% GNU General Public License for more details.
%
% You should have received a copy of the GNU General Public License
% along with this program.  If not, see <http://www.gnu.org/licenses/>.


[ details ] = biasd_subjects( id , OPTIONS);
load(details.startfile)

load(fullfile(OPTIONS.designdir, 'cue_validity.mat'));
D.validity_m = nan(height(D), 1);
D.validity_m(D.task==2) = u;
D.validity_m(D.timeout==1) = NaN;

disp([id, ': NaN_v=', num2str(sum(isnan(D.cue_validity(D.task==2)))), ', NaN_m=', num2str(sum(isnan(D.validity_m(D.task==2)))), '/', num2str(sum(D.timeout==1 & D.task==2))]);

save(details.analysisfile,'D')





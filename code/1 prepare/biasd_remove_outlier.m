function D = biasd_remove_outlier(id, OPTIONS)
% Removes zero entries and 2.5% longest and 2.5% shortest RT

% Author: Maya Schneebeli 
% Reviewer: Annia R�esch

% Copyright (C) 2020 TNU, Institute for Biomedical Engineering, University of Zurich and ETH Zurich.

% This file is released under the terms of the GNU General Public
% Licence (GPL), version 3. You can redistribute it and/or modify it under the terms of the GPL
% (either version 3 or, at your option, any later version). 
%
% This program is distributed in the hope that it will be useful,
% but WITHOUT ANY WARRANTY; without even the implied warranty of
% MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
% GNU General Public License for more details.
%
% You should have received a copy of the GNU General Public License
% along with this program.  If not, see <http://www.gnu.org/licenses/>.


[ details ] = biasd_subjects( id , OPTIONS);
load(details.startfile)

% find outlier
% Outlier are replaced by NA
disp(' ')
disp('----------------------------------------------')
disp(['Remove outlier for subject ' details.subjectname ' ...']);
disp('----------------------------------------------')
disp(' ')
for i = 1:2
    disp(['Task', num2str(i)]);
    E{i} = D(D.task==i,:); %access the two tasks separetely
    idx_zero = isnan(E{i}.response); %outlier1: missing responses
    
    %outlier2: keep only 95% confidence interval
    quants = quantile(E{i}.rt(~isnan(E{i}.rt)),[.025, .975]); 
    idx_outlier = E{i}.rt < quants(1) | E{i}.rt > quants(2); 
    E{i}.outlier = idx_outlier;
    
    idx_nans = idx_zero | idx_outlier;
    E{i}.response( idx_nans) = NaN;
    E{i}.rt(idx_nans) = NaN;
    E{i}.correct( idx_nans) = NaN;
    E{i}.cue_validity (idx_nans) = NaN;
    disp([' - missing trials: ', num2str(sum(idx_zero))]);
    disp([' - correct trials: ', num2str(nansum(E{i}.correct)),' (', num2str(nansum(E{i}.correct)/nansum(~isnan(E{i}.correct))), '%)']);
    
end
    
D = [E{1};E{2}];


save(details.analysisfile,'D')
disp(['Remove outlier for subject ' details.subjectname ': completed']);

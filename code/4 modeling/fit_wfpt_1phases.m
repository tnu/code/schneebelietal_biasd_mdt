function logp = fit_wfpt_v2(D,x)
% This script fits data using the Wiener first passage time
% distribution (WFPT) from Navarro et al 2009.

% Author: Maya Schneebeli 
% Reviewer: Annia R�esch

% Copyright (C) 2020 TNU, Institute for Biomedical Engineering, University of Zurich and ETH Zurich.

% This file is released under the terms of the GNU General Public
% Licence (GPL), version 3. You can redistribute it and/or modify it under the terms of the GPL
% (either version 3 or, at your option, any later version). 
%
% This program is distributed in the hope that it will be useful,
% but WITHOUT ANY WARRANTY; without even the implied warranty of
% MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
% GNU General Public License for more details.
%
% You should have received a copy of the GNU General Public License
% along with this program.  If not, see <http://www.gnu.org/licenses/>.

ze_v = x(1);
ze_a = x(2);
mu1hat = x(3);
ze_t = x(4);

%% Define the perceptual model
% development of starting point z over time
% TODO: implement HGF_sim here to get vector z

l = height(D);
%version valid/invalid as boundaries
% pressed = D.response(p+1:p+ph(i)) ==  D.truecue(p+1:p+ph(i));

%version left/right as boundaries 
pressed = D.response;

%version valid/invalid as boundaries
% if D.validity == 1
%     c = D.stim(p+1:p+ph(i));
% else
%     c = -D.stim(p+1:p+ph(i));
% end

%version left/right as boundaries
c = D.stim;

vf = ze_v*c; % v: drift rate 
af = repmat(ze_a, l,1); % a: upper boundary

tf = (D.rt-ze_t);

err = 10^(-29); % err: error threshold
s = 1; % Variance/Noise of random walk



%upper boundary hit = right response
%lower boundary hit = left response

% Mu1hat = Bias towards phase validity (phase congruent response)
mu1hat = abs(D.phasevalidity-mu1hat);

if all(isnan(D.truecue))
    zf = mu1hat.*af;
else
    % D.truecue is 1 if cue points to right, and 0 if cue points to left side
    % Convert bias from phase congruent response to left/right response
    % and scale it with boundary
    zf = (1-abs(D.truecue-mu1hat)).*af;
end

% Equation for answers according to Navarro & Fuss 2009

%probability of hitting upper boundary
py_upperf =(exp(2.*af.*vf./s.^2)-exp(2*(af-zf).*vf./s.^2))./...
(exp(2.*af.*vf./s.^2)-1);

%probability of response time upper boundary
pt_upperf = wfpt_wrapper(tf,-vf,...
    af,(af-zf),err);

%probability of response time lower boundary
pt_lowerf = wfpt_wrapper(tf,vf,...
    af,zf,err);

%probability of response time given the boundary hit (upper or lower)
ptf = pressed.*pt_upperf+...
    (1-pressed).*pt_lowerf;

%log likelihood
logp = -nansum(log(ptf)) ;






end


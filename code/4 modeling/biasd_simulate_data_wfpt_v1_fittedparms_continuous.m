function sim = biasd_simulate_data_wfpt_v1_fittedparms_continuous(id, OPTIONS)
% This script simulates data using the Wiener first passage time
% distribution (WFPT) from Navarro et al 2009.

% Author: Maya Schneebeli 
% Reviewer: Annia R�esch

% Copyright (C) 2020 TNU, Institute for Biomedical Engineering, University of Zurich and ETH Zurich.

% This file is released under the terms of the GNU General Public
% Licence (GPL), version 3. You can redistribute it and/or modify it under the terms of the GPL
% (either version 3 or, at your option, any later version). 
%
% This program is distributed in the hope that it will be useful,
% but WITHOUT ANY WARRANTY; without even the implied warranty of
% MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
% GNU General Public License for more details.
%
% You should have received a copy of the GNU General Public License
% along with this program.  If not, see <http://www.gnu.org/licenses/>.


details = biasd_subjects(id, OPTIONS);


load(details.startfile);

stim = -.55:.01:.55;
coh = 0:.01:.55;


for task = 1:2
 sim{task}.stim = stim;
 sim{task}.coh = coh;
 sim{task}.wfpt_v1_rt_sim = nan(length(stim),1);
 sim{task}.wfpt_v1_response_sim = nan(length(stim),1);
   
 
   
    l = length(stim); % number of simulation trials
    
    
    ze_a = mean(D.wfptfit_task_v1_ze_a(D.task == task));
    ze_v = mean(D.wfptfit_task_v1_ze_v(D.task == task));
    ze_t = 1000*mean(D.wfptfit_task_v1_ze_t(D.task == task));
    mu1hat = mean(D.wfptfit_task_v1_mu1hat(D.task == task));
    
    
    
    dt = 0.001;
    t = [10^-80 dt:dt:10];% t: time range
    t= t';
    
    v = ze_v*stim; % v: drift rate
    a = repmat(ze_a,l,1); % a: upper boundary
    
    err = 10^(-29); % err: error threshold
    s = 1; % VarianceNoise of random walk
    
    z = (1-abs(1-mu1hat)).*a;
    
    
    rtsample = zeros(1,l);
    rtmean = zeros(1,l);
    responsemean = zeros(1,l);
    
    for ii = 1:l
        
        
        pt_upper = wfpt_wrapper(t,-repmat(v(ii),length(t),1),...
            repmat(a(ii), length(t),1),a(ii)-repmat(z(ii),length(t),1),err);
        
        pt_lower = wfpt_wrapper(t,repmat(v(ii),length(t),1),...
            repmat(a(ii), length(t),1),repmat(z(ii),length(t),1),err);
        
        
        
      
    
        x = [-flip(t);t];
        x1 =[flip(t);t];
        pt = [flip(pt_lower); pt_upper];
        pt_norm = pt./sum(pt);
%         for jj = 1:10

        rn = rand;
        pt_cum = cumsum(pt_norm);
        [~, min_ind] = min(abs(pt_cum-rand));
        rtsample(ii) = x(min_ind);
        

%         rtsample(ii) = randsample(x,1,true,pt); %sample randomly from distribution
        
        disp(['accumulated probability:',num2str(sum(pt))])
        rtmean(ii) = sum(x1.*pt_norm)+ze_t/1000;
        responsemean(ii) = sum(pt_upper/sum(pt));

%         end
    end
%     rtsample = mean(rtsample);
    
    response = (1+sign(rtsample))./2;
    rt = abs(rtsample)+ze_t/1000; %sample randomly from distribution
    
    response(rt > 2.5) = NaN;
    rt(rt > 2.5) = NaN;
    
    sim{task}.wfpt_v1_rt_sim = rtmean';
    sim{task}.wfpt_v1_response_sim = responsemean';
    sim{task}.wfpt_v1_correct_sim = [responsemean(ceil(end/2)), ...
        (1-flip(responsemean(1:floor(end/2)))+responsemean(ceil(end/2)+1:end))./2];
    
   
%     figure;
%     for i = 1:length(unique(abs(D.stim)))
%         st = unique(abs(D.stim));
%         
%         subplot(2,2,i)
%         histogram(D.rt(abs(D.stim)==st(i)&D.task ==task&D.correct>=0.5), 'BinWidth', .1, 'Normalization', 'count'); hold on; 
%         histogram(D.rt(abs(D.stim)==st(i)&D.task ==task&D.correct<=0.5), 'BinWidth', .1, 'Normalization', 'count'); hold on; 
% 
%         pt_up = wfpt_wrapper(t,-repmat(ze_v*st(i),length(t),1),...
%             repmat(ze_a, length(t),1),ze_a-repmat(ze_a*mu1hat,length(t),1),err);
%         
%         pt_low = wfpt_wrapper(t,repmat(ze_v*st(i),length(t),1),...
%             repmat(ze_a, length(t),1),repmat(ze_a*mu1hat,length(t),1),err);
%         plot(t+ze_t/1000, pt_up*4); 
%         plot(t+ze_t/1000, pt_low*4); 
%     end
%     
    
    
        
    
end



save(details.analysisfile,'sim')


end


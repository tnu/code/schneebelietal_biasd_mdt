function [ D, results ] = biasd_fit_wfpt_v1( id, OPTIONS)
% This script fits data using the Wiener first passage time
% distribution (WFPT) from Navarro et al 2009.

% Author: Maya Schneebeli 
% Reviewer: Annia R�esch

% Copyright (C) 2020 TNU, Institute for Biomedical Engineering, University of Zurich and ETH Zurich.

% This file is released under the terms of the GNU General Public
% Licence (GPL), version 3. You can redistribute it and/or modify it under the terms of the GPL
% (either version 3 or, at your option, any later version). 
%
% This program is distributed in the hope that it will be useful,
% but WITHOUT ANY WARRANTY; without even the implied warranty of
% MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
% GNU General Public License for more details.
%
% You should have received a copy of the GNU General Public License
% along with this program.  If not, see <http://www.gnu.org/licenses/>.


[ details ] = biasd_subjects( id , OPTIONS);
load(details.startfile)

disp(['Fitting WFPT (fixed starting point 0.5) for subject TNU_BIASD_', id, '...'])

data = D;
D_temp = table();

for i = unique(D.task)'
    
D = data(data.task == i,:);

D.wfptfit_task_v1_ze_v = nan(height(D),1);
D.wfptfit_task_v1_ze_a = nan(height(D),1);
D.wfptfit_task_v1_mu1hat = nan(height(D),1);
D.wfptfit_task_v1_ze_t = nan(height(D),1);

D.wfptfit_task_v1_llik = nan(height(D),1);
D.wfptfit_task_v1_bic = nan(height(D),1);

logp = @(x)(fit_wfpt_v1(D,x));

options = optimoptions('ga','PlotFcn', @gaplotbestf, ...
    'FunctionTolerance', 10^(-4), 'MaxGenerations', 4000, ...
    'InitialPopulationRange', [.1,50;.1,4;.5,.5;0,1 ]');
%options = optimoptions('ga', 'FunctionTolerance', 10^(-5), 'MaxGenerations', 2000);
%lower and upper bounds for ze_v, ze_a, mu1hat, ze_t
LB = [10^(-6), .01, .5, 0];
UB = [Inf,    Inf, .5, Inf];%min(D.rt)*1000-10^(-6)];
%LB = [10^(-6), 1, 0];
%UB = [Inf, 1, min(D.rt)*1000];
[x,fval,exitflag,output] = ga(logp,4,[],[],[],[],LB,UB, [],[], options);





results.wfptfit.task{i}.x = x;
results.wfptfit.task{i}.fval = fval;
results.wfptfit.task{i}.exitflag = exitflag;
results.wfptfit.task{i}.output = output;

D.wfptfit_task_v1_ze_v = x(1)*ones(height(D),1);
D.wfptfit_task_v1_ze_a = x(2)*ones(height(D),1);
D.wfptfit_task_v1_mu1hat = 0.5*ones(height(D),1); 
D.wfptfit_task_v1_ze_t = x(4)*ones(height(D),1);
D.wfptfit_task_v1_llik = fval*ones(height(D),1);
D.wfptfit_task_v1_bic = log(2*height(D))*3-2*fval*ones(height(D),1);

disp(['Drift v: ', num2str(x(1))]);
disp(['Boundary b: ', num2str(x(2))]);
disp('Starting point: 0.5 (fixed)');
disp(['Non-decision time: ' num2str(x(4))]);
disp(' ')
disp(['Llik: ', num2str(fval)]);


D_temp = [D_temp;D];
end

D = D_temp;

save(details.analysisfile,'D', 'results');

disp(['Fitting WFPT (fixed starting point 0.5) for subject TNU_BIASD_', id, ': complete'])

end


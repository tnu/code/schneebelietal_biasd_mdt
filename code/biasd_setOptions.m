function OPTIONS = biasd_setOptions(OPTIONS, startdir, start, analysisdir, analysis)
% definition of startfile and analyisfile 

% INPUT
% - startdir: folder of (input) startfile
% - start: name of (input) startanalysis
% - analysisdir: folder of (output) analysisfile
% - analyisis: name of (output) analysis
% OUTPUT
% - updated struct OPTIONS

% Author: Maya Schneebeli 
% Reviewer: Annia R�esch

% Copyright (C) 2020 TNU, Institute for Biomedical Engineering, University of Zurich and ETH Zurich.

% This file is released under the terms of the GNU General Public
% Licence (GPL), version 3. You can redistribute it and/or modify it under the terms of the GPL
% (either version 3 or, at your option, any later version). 
%
% This program is distributed in the hope that it will be useful,
% but WITHOUT ANY WARRANTY; without even the implied warranty of
% MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
% GNU General Public License for more details.
%
% You should have received a copy of the GNU General Public License
% along with this program.  If not, see <http://www.gnu.org/licenses/>.


%define startfile and analysisfile
OPTIONS.startdir = startdir;
OPTIONS.start = start;
OPTIONS.analysisdir = analysisdir;
OPTIONS.analysis = analysis;



end
function y = conf95(x)
    SEM = std(x)/sqrt(length(x));               % Standard Error
    ts = tinv(0.95,length(x)-1);
    y = ts*SEM;  
end
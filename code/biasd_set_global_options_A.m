
function OPTIONS = biasd_set_global_options_A()
%INPUT: No input
%OUTPUT: OPTIONS which contains data directories and subject ids for the
%groups. 

%IMPORTANT: adjust working directory below in "set_local_directory"
%The working directory contains the following folders:
%analysed: folder with analysed data
%code: folder with code
%design: folder with file cue_validity.mat

% Author: Maya Schneebeli 
% Reviewer: Annia R�esch

% Copyright (C) 2020 TNU, Institute for Biomedical Engineering, University of Zurich and ETH Zurich.

% This file is released under the terms of the GNU General Public
% Licence (GPL), version 3. You can redistribute it and/or modify it under the terms of the GPL
% (either version 3 or, at your option, any later version). 
%
% This program is distributed in the hope that it will be useful,
% but WITHOUT ANY WARRANTY; without even the implied warranty of
% MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
% GNU General Public License for more details.
%
% You should have received a copy of the GNU General Public License
% along with this program.  If not, see <http://www.gnu.org/licenses/>.


OPTIONS = set_local_directory;

OPTIONS.analysisdir = [];
OPTIONS.startdir = [];
OPTIONS.analysis = [];
OPTIONS.start = [];
OPTIONS.analysisstepsdir = [];
OPTIONS.analysisstepsfile = [];

%Id numbers for ASD group

sP = {
    '6093'    '0548'    '2079'    '7429'    '6469'    '1642'    '7963' ...
    '9764'    '1722'    '3737'    '2200'    '0547'    '3850'    '7225' ...
    '1780'    '0321'    '2960'    '3225'    '0466'    '4028'    '5324' ...
    '0358'    '8535'    '2617'    '6361'    '0046'    '2735'    '6489' ...
    '0155'    '3978'    '0853'    '3752'    '6816'    '1640'    '7579' ...
    '4652'    '5550'    '4165'    '9063'    '1336'    '0617'    '0074' ...
    '4370'    '1553'    '9881'    '2748'    '6617'  ...
    };
    
    
%excluded because of performance (<68% corr or >10%missings)
sPex_corr = { '1486'    '9492'};
%excluded because of performance (<80% in coh .128 & .512)
sPex_corr80 = { '0956'    '5791'    '6284'};



         
%ID numbers controls
   
sC = {

    '2424'    '9095'    '6090'    '1767'    '3156'    '7968'    '1124' ...
    '9128'    '3817'    '1790'    '3900'    '2621'    '1821'    '8982' ...
    '8939'    '6438'    '5959'    '8409'    '7073'    '4148'    '8904' ...
    '5984'    '1973'    '7008'    '5994'    '8132'    '4191'    '2862' ...
    '4057'    '1340'    '4425'    '7278'    '5626'    '3562'    '7499' ...
    '8965'    '2681'    '4580'    '2413'    '2015'    '0461'    '7654' ...
    '6751'    '8133'    '2723'    '3472'    '9317'    '2869'    '8540' ...
    '6158'  };


% excluded because of performance (<65% correct)
sCex_corr = {'5753'};
   
 
   

%All included AG
OPTIONS.AGIDs = sP;
%Alll included healthy controls
OPTIONS.CGIDs = sC;

%all included subjects before preprocessing (i.e. all with no missing data)
OPTIONS.subjectIDsAll = [sC, sCex_corr, sP, sPex_corr, sPex_corr80];
OPTIONS.subjectIDs = [sC, sP];

OPTIONS.groups = {'AGIDs','CGIDs'};


end

function OPTIONS = set_local_directory
    
    % Working directory
    cd .. %navigate one folder up
    wd = pwd; %project folder: '...\schneebelietal_biasd_mdt\'
    OPTIONS.workdir = wd;

      % Design directory
    design = [wd,'\design\'];
    OPTIONS.designdir = design;
    
    %Directory of analysed data
    if ~exist([wd, '\analysed\'], 'dir')
    mkdir([wd, '\analysed\']);
    end


   %add all paths to current workspace
    addpath(genpath(OPTIONS.workdir));
    addpath(genpath(OPTIONS.designdir));
    cd(OPTIONS.workdir);
    
end
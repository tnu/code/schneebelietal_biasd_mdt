# BIASD_Code

This is the code of the manuscript: Disentangling “Bayesian brain” theories of autism spectrum disorder from M. Schneebeli, H. Haker, S. Iglesias, A. Rüesch, N. Zahnd, S. Marino, G., F. H. Petzschner,  K. E. Stephan

The project is structured as follows:

* code: contains all MATLAB code, i.e. preprocessing, statistics (excluding general linear mixed effects models), modeling procedures, and plots
* raw: empty folder; raw data will be available on ETH Research Collection as soon as the manuscript is accepted by a peer-reviewed journal
* design: contains task design, i.e. trance of valid and invalid trials
* rcode: contains all R code, i.e. linear mixed effects models
* analysed: is created during the analysis and contains data results after each processing step

To run the code, do following:

* MATLAB: Start in the code folder. Specify your local path in the file code\biasd\_set\_global\_options.  Then run the biasd\_master.m file. A text file is created with all processing steps and the results.
* R: Next, go to the rcode folder. Specify your local path in the file biasd\_master\_r.R and run the code. A text file is produced with the results. 

The code runs on MATLAB version 2016b and Rstudio version 3.5.2.


